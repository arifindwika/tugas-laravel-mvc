<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    // public function register(){
    //     return view('register');
    // }

    public function register(){
        return view('data.register');
    }

    public function welcome(Request $request){
        // return "Halo";
        return view('welcome');
    }

    public function welcome_post(Request $request){
        // dd($request->all());
        $first_name = $request["first_name"];
        $last_name = $request["last_name"];
        // return "$first_name";
        // return "$last_name";
        return view('data.welcome', compact('first_name','last_name'));
    }
}
