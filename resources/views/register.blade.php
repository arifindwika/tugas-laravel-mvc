<!DOCTYPE html>
<html>
<head>
	<title>Sign Up</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>

	<h2>Sign Up Form</h2>
	<form action="/welcome" method="POST">
         @csrf
		<label>First Name :</label><br>
		<input type="text" name="first_name"><br><br>

		<label>Last Name :</label><br>
		<input type="text" name="last_name"><br><br>

		<label>Gender :</label><br>
		<input type="radio" name="gender">Male <br>
		<input type="radio" name="gender">Female <br>
		<input type="radio" name="gender">Other <br><br>

		<label>DOB :</label><br>
		<input type="date" name="dob"><br><br>

		<label>Language Spoken :</label><br>
		<input type="checkbox" name="lang">Bahasa Indonesia <br>
		<input type="checkbox" name="lang">English <br>
		<input type="checkbox" name="lang">Japanese <br>
		<input type="checkbox" name="lang">Spanish <br>
		<input type="checkbox" name="lang">Other <br><br>

		<label>Nationality :</label><br>
		<select name="'nationality">
			<option></option>
			<option value="indonesian">Indonesian</option>
			<option value="singaporeans">Singaporeans</option>
			<option value="malaysians">Malaysians</option>
			<option value="thais">Thais</option>
			<option value="other">Other</option>
		</select> <br><br>

		<label>Bio :</label><br>
		<textarea name="bio" cols="30" rows="10" placeholder="Write Here...."></textarea><br><br>

		<input type="submit">
			
	</form>
</body>
</html>